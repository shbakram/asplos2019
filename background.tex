\section{Background}
\label{background}

This section describes background on the Immix garbage collector and
Jikes, our Java virtual machine (JVM), which we modify. We also
describe the state-of-the-art Kingsguard write-rationing collector,
which uses dynamic write profiling and to which we compare.
% configuration we use as a starting point for Crystal Gazer and which we
% evaluate against.  

\paragraph{Immix.} We build upon the default best-performing collector
in the literature,
generational Immix (GenImmix)~\cite{immix-blackburn,SBYM:13}.  Fresh allocation goes into a
fixed-sized contiguous nursery, which uses bump-pointer allocation.  During a
nursery collection, it copies live objects to a mark-region mature space and
reclaims the entire nursery, readying it for new allocation.  The mark-region
space is organized into a hierarchy of coarse-grained blocks, which are multiples of the page
size, consisting of finer-grained lines, which are multiples of the cache line size.
% Multiple lines fit into a block.   
Objects which survive nursery collections
are copied contiguously in to free lines, first in partially-occupied blocks, then
in completely free blocks.  Immix full heap collections mark live
objects, lines, and blocks; reclaiming completely free blocks and
completely free lines to create
partially-occupied blocks.  Immix uses unsynchronized per-thread
allocators that obtain
partially full and completely empty blocks
from a global allocator. It uses multiple garbage collection threads
with a global work queue.

We use default Immix settings: lines are 256\;Bytes (matching the PCM
line size), blocks are 32\;KB, and \emph{large objects} ($\geq$ 
8\;KB) are allocated in a separate non-moving  Large Object Space (LOS).
Managed heaps treat large objects specially to avoid high copying costs.

Sizing the nursery is a space-time tradeoff.  With large nurseries, a nursery
collection takes longer, pausing applications longer.  With small
nurseries, pauses are shorter and the memory
footprint is smaller, but the nursery is collected more often.   Because Java
requires zero-initialization and applications allocate at high rates,
the nursery is highly mutated.  

\paragraph{Kingsguard collectors.} Previous work proposed two
Write-Rationing garbage collection, called Kingsguard-Nursery (KG-N)
and Kingsguard-Writers (KG-W)~\cite{ration}.  KG-N) allocates nursery
objects into DRAM memory, because nursery objects are the most highly
mutated.  All objects that survive a nursery collection are promoted
(copied) to PCM memory to utilize its capacity.  KG-N improves the
lifetime of a hybrid memory system, in comparison with a PCM-only
system, by 5x.  

KG-W seeks to reduce writes further by dynamically
profiling writes to objects that survive a nursery collection. It
always first copies survivors to a DRAM observer space.  Using a
relatively expensive write barrier, KG-W detects writes to observer
space objects.  When it collects the observer space, it copies objects
marked as having been written \emph{even once} since the last
collection to a mature DRAM space, assuming they will be written again
in the future. It copies all others a PCM mature space.  The mature
spaces are also monitored for writes, and during a full-heap
collection, objects are moved between the two spaces.  The observer
space \emph{both} gives objects more time to die before being copied
to DRAM or PCM, and the system time to observe write behavior.

KG-W includes two additional optimizations.  The large object
optimization (LOO) optionally allocates some large objects (if the
allocation rate of large objects is higher than the nursery allocation
rate) in the nursery if they are small enough to fit in half of the
remaining nursery.  The LOO gives them a chance to die before being
placed in PCM.  Otherwise, they are placed directly into a LOS backed
by PCM to use the large capacity of PCM.  However, if they incur
writes, during the next full-heap garbage collection, they will be
moved (thus incurring copying costs) to a DRAM LOS.  The metadata
optimization (MDO) eliminates PCM writes to mark bits for object in
PCM during a collection, by storing PCM object metadata in DRAM.

KG-W improves PCM's lifetime in a hybrid memory system by 11x compared
to PC-only, but incurs a 40\% execution time overhead compared to a
DRAM-only system.  Furthermore, KG-W is reactive, and assumes writes
to objects in the observer space will predict future writes.  However,
objects are only monitored for a short period of time in the
observer's space.  Although the mature spaces are also monitored for
writes, full heap collections happen infrequently and incur more
copying costs.  Lastly, LOO puts large objects that don't fit in the
nursery directly into the PCM LOS space, which will be detrimental for
highly-written large objects. \cgz uses offline profiling to
predict writes, eliminating the high cost of dynamic monitoring and
the multiple unnecessary copying. \cg achieves a combination of better
performance, lower use of DRAM, and fewer writes to PCM compared to
KG-W, the previous state of the art.


