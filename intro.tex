\section{Introduction}
\label{intro}

The ever-increasing complexity of manufacturing DRAM is limiting supply and
hiking main memory cost. According to semiconductor research analysts, between
the year 2017 and 2018, DRAM price per giga bit increased by 50\% whereas the
year-over-year bit volume growth continued to drop.  Main memory supply trends
are especially worrisome because applications have an insatiable
desire for memory. %footprints, and we need to process large amounts
                   %of data.  
Expecting further DRAM supply shortages, Facebook is already
experimenting with non-volatile memory (NVM) as a DRAM
replacement~\cite{Eisenman:2018}.

Production NVM uses phase-change materials for storing information and offers
five advantages: byte-addressability, high capacity, low idle power, and
non-volatility, but has two major drawbacks compared to DRAM: longer
latency, and limited write
endurance~\cite{lee-pcm-isca,lee-pcm-micro,PCM-Vacum-Science}.
Advancements in materials and manufacturing may eventually bridge the DRAM-PCM
latency gap~\cite{ITRS-PCM-LATENCY,numonyx}.  Endurance is a harder problem
because each PCM write changes the material form~\cite{PCM-Vacum-Science}.
Prototype PCM hardware has an endurance between 1\:M to 100\:M writes per
cell~\cite{lee-pcm-isca,lee-pcm-micro,after-hard-drives,mellow-writes,AthmanathanSPPE16}.
Manufacturers today only guarantee it up to 140\:MB per second~\cite{Eisenman:2018}.

Due to its persistent and byte-addressable features, PCM will serve both as
main memory and persistent storage.  Both roles make write
endurance a challenge. This paper focuses on making PCM a practical DRAM
replacement without requiring changes to applications  and programming
models for managed languages. We target hybrid DRAM-PCM memories since PCM on its own is unable to
replace DRAM~\cite{lee-pcm-micro,moin-pcm-isca,lee-pcm-isca}.  Hybrid memories
combine DRAM and PCM seeking the best of both memory technologies. Our goal
is to mitigate PCM wear-out and improve its lifetime by placing highly written
data in DRAM, and the bulk of mostly-read data in PCM. 


\begin{figure*}[t]
        \centering
        \subfloat[Pjbb] {\includegraphics[width=8cm]{./figures/GPjbb.pdf}}
	\vspace{1mm}
        \subfloat[HSqldb] {\includegraphics[width=8cm]{./figures/GHSqldb.pdf}}
        \caption{Distribution of old space writes and heap volume by allocation site.
                 \textit{A few sites capture a majority of objects with old space writes and a small fraction of the old space.}
        }
        \label{fig:old-space-distributions}
\end{figure*}

A large body of prior work proposes hardware wear-leveling techniques that
spreads writes out across the entire PCM capacity to mitigate PCM
wear-out~\cite{moin-pcm-isca,Qureshi:2009:ELS,Qureshi:2011:PLH}.  A few works
explore write-limiting using the OS on top of
wear-leveling~\cite{bruce-os-hybrid-memory,ricardo-os,Zhang:2009}.  
Recently, researchers found that for many Java applications: (1)
wear-leveling and wear-limiting doe not lead to practical PCM lifetimes~\cite{ration}. % and (2) existing
% OS approaches for write-limiting are inefficient because they operate at a
% coarse-grained page granularity~\cite{ration}. 
This motivated researchers to
propose write-rationing garbage collectors that place highly written objects in
DRAM and the rest in PCM~\cite{ration}.

Unfortunately, write-rationing garbage collectors suffer from three drawbacks:
(1) dynamically discovering which objects get writes degrades performance, (2)
object writes are dynamically profiled in a limited time window leading to
mispredictions and unnecessary writes to PCM, (3) they consume sub-optimal DRAM
capacity to keep highly written objects away from PCM. 

This paper introduces profile-driven write-rationing garbage
collectors (GC) for hybrid
memories. Our systems use knowledge about which
allocation sites produce highly written objects during a previously
gathered profile of an application. They 
then proactively places objects in DRAM and PCM according to their
predicted write and liftetime behaviors.
Our GC design is motivated by the observation that allocation sites are highly
accurate predictors of mature object writes. 
Figure~\ref{fig:old-space-distributions} shows the distribution of writes to
mature space objects and the percentage of volume they occupy on a per
allocation site basis for two representative Java benchmarks. In both
cases, a small number of sites on the X-axis capture 99\% of mature
writes and occupy less than 20\% of the heap volume.

We introduce Crystal Gazer (CG), which uses profile-based prediction
of per allocation-site write intensity and lifetimes to place objects
in either PCM or DRAM. CG is proactive and thus improves execution
time compared to the Kingsguard collectors in prior work that
dynamically monitor objects in a surviver space~\cite{ration}.  CG
combines write prediction with prior work on object lifetime
prediction~\cite{Blackburn:2007:PP} to both allocate and promote small
and large objects.  This approach exposes a Pareto optimization problem
for tuning \cg to optimize for either aggressively reducing writes to
PCM or reducing the use of DRAM capacity.

\begin{figure*}[t]                             
        \centering                            
        \subfloat[DaCapo] {\includegraphics[width=5cm]{./figures/GParetoD.pdf}}
	\hspace{1mm}
        \subfloat[Pjbb] {\includegraphics[width=5cm]{./figures/GParetoP.pdf}}
	\hspace{1mm}
        \subfloat[GraphChi] {\includegraphics[width=5cm]{./figures/GParetoG.pdf}}
        \caption{Reduction in PCM writes normalized to KG-N versus DRAM consumed by KG-W and CG. 
                 \textit{CG provides the flexibility of trading off PCM writes
                 for DRAM capacity and vice-versa.}}
        \label{fig:pareto}                      
\end{figure*}  
 
We compare to prior write-rationing garbage collectors for hybrid
memories~\cite{ration}. Kingsguard-nursery (\kgn) places the highly mutated
nursery in DRAM and the mature space in PCM. Kingsguard-writers (\kgw)
dynamically monitors writes to nursery survivors in a new observers space and
includes optimizations to protect PCM from writes to large object and collector
metadata.  We evaluate these collectors using a hybrid memory emulation
platform~\cite{arxiv:jikes-emul-hybrid} that uses commodity multi-socket NUMA
machines to speed up evaluation. The emulation results reflect the same trends
as simulator and execution time results~\cite{arxiv:jikes-emul-hybrid}.
Figure~\ref{fig:pareto} shows \cg configurations use less DRAM and have fewer
PCM writes than \kgw and \kgn normalized to \kgn for three Java benchmark
groups. In all cases, \cg operates on a tradeoff space of PCM writes and DRAM
capacity, whereas \kgn and \kgw consume fixed amounts of DRAM to limit PCM
writes.  Neither \kgn or \kgw have knobs to control write intensity heuristics
to satisfy varying requirements.  \cg configurations control this tradeoff by
varying in how they use the offline advice to classify sites, either to favor
PCM lifetime or DRAM capacity.


\noindent In summary, the contributions of this work are:

\begin{itemize} 

\item analysis showing a few allocation sites get majority of writes in managed heaps;

\item a predictor for hybrid memories that accurately predicts writes to
objects and object lifetimes on a per allocation-site basis;

\item the design and implementation of profile-driven write rationing garbage
collectors that allocate and promote objects to DRAM or PCM in hybrid memories using an allocation-site predictor;

\item heuristics that classify allocation sites as write-intensive or
non-write-intensive to optimize for PCM writes and DRAM capacity;

\item emulation results showing better execution time and reduction in PCM
lifetimes compared to state-of-the-art garbage collectors for hybrid memories;

\item a low-overhead approach to use emerging non-volatile memories as main
memories without requiring changes to the application or the
programming model for managed languages.

\end{itemize}


