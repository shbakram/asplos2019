\begin{abstract}

Non-volatile memories (NVM) offer greater capacity than DRAM but suffer from
high latency and low write endurance. Hybrid memories combine DRAM and NVM to
form scalable memory systems with high capacity, low energy, and high
endurance. How best to mitigate NVM wear-out to extend NVM lifetime is an open
question.  This work mitigates NVM wear-out to make NVM a practical DRAM
replacement by leaving the application and the programming model unchanged.

Prior approaches use OS and garbage collectors to place highly written pages
and objects in DRAM to limit NVM writes. This paper introduces a new class of
profile-driven garbage collectors that predict writes to objects in managed
applications based on offline knowledge. Profile-driven garbage collectors
exploit the surprising predictability of writes on a per allocation-site basis
to place highly written objects in DRAM and the rest in NVM. 

We implement a profile-driven garbage collector called Crystal Gazer (CG) for
the Java programming language and evaluate it on 16 real Java applications.
Crystal Gazer first profiles a Java application, collecting write intensity
statistics on a per allocation-site basis.  Then in a production run of the
application, all objects from a write-intensive allocation site are marked to
be placed in DRAM, which offers faster latency and no wear-out.  Since most
objects are not write-intensive, the rest go in the large capacity NVM, whose
lifetime is extended.  Because Crystal Gazer collects application profiles
offline, it adds negligible overhead than the previous state-of-the-art, while
offering new tradeoffs between the amount of DRAM used, and the aggressiveness
of limiting writes to PCM.  We evaluate CG on a new emulation platform for
hybrid memories that we propose. Compared to existing garbage collectors for
hybrid memories, CG offers new tradeoffs in how best to manage a hybrid memory
system using the language runtime, balancing execution time overhead, writes
to NVM and DRAM capacity.  


  Non-volatile memories (NVM) offer greater capacity than DRAM but
  suffer from high latency and low write endurance. Hybrid memories
  combine DRAM and NVM to form scalable memory systems with the
  promise of high capacity, low energy, and high endurance.  While
  many are exploring the persistance properties of NVM, this paper
  explores the use of NVM to augment DRAM capacity without changes to
  applications and managed language programming models. We address the
  challenge of how best to exploit NVM capacity while mitigating NVM
  wear-out to extend NVM lifetime.

  This paper introduces profile-driven write-rationing garbage
  collectors. These systems predict allocation sites that produce
  frequently written objects based on previous program
  executions. They allocate nursery objects in DRAM and promote
  longer-lived highly written objects in DRAM. They allocate directly
  and promote longer-lived read-mostly objects in NVM. The approach is
  effective because (1) writes are surprising predictability on a per
  allocation-site basis for the 16 Java benchmarks we examine, and (2)
  only a small fraction of objects are highly written.  Our system
  thus meets three important goals: it performs well; it maximizes the
  use of NVM for its capacity; and it exceeds the NVM write-rate
  requirements specified by manufactorers.
\end{abstract}

