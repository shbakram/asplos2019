#
# makefile for generating paper.ps
#

#LATEX = latex2e
#LATEX = latex
LATEX = pdflatex

FILES   = $(shell ls *.tex *.bib)

default:  $(FILES) $(FIGS) paper.bbl 
	-$(LATEX) paper
	-bibtex paper
	-$(LATEX) paper
	-bibtex paper
	-$(LATEX) paper
	-$(LATEX) paper

paper.pdf: $(FILES) paper.bbl
	-$(LATEX) paper

paper.bbl: $(FILES) $(FIGS)
	-$(LATEX) paper
	-bibtex paper.aux


clean:
	rm -f *.log *.aux *.blg *.bbl *.out paper.pdf

wipe: commit clean
	rm -f $(FILES)  Makefile

wc:
	@cat paper.tex | detex | wc -w

commit:
	svn commit

update:
	svn update

status:
	svn status

